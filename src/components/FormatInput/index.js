import React, { Fragment } from 'react'
import { View, Text, StyleSheet, TextInput } from 'react-native'
import { useFormContext, Controller } from 'react-hook-form'
import { mask, unMask } from 'react-native-mask-text'

const FormatInput = ({
  name,
  label,
  format,
  style,
  errorInside = false,
  getValue,
  ...props
}) => {
  const { control, errors } = useFormContext()

  const styles = StyleSheet.create({
    label: {
      fontSize: 10,
      color: '#c0c0c0',
    },
    input: {
      borderBottomWidth: 1,
      borderBottomColor: '#cdcdcd',
    },
    inputBox: {
      marginBottom: errorInside && errors[name] ? 24 : 8,
    },
    errorBox: {
      position: errorInside ? 'absolute' : undefined,
      left: errorInside ? 0 : undefined,
      bottom: errorInside ? -16 : undefined,
    },
    errorMessage: {
      color: '#d5462d',
    },
  })

  return (
    <View style={style}>
      {label && <Text style={styles.label}>{label}</Text>}
      <Controller
        name={name}
        control={control}
        render={({ field: { onChange, value } }) => (
          <Fragment>
            <View style={styles.inputBox}>
              <TextInput
                placeholderTextColor="#c0c0c0"
                onChangeText={(text) => {
                  onChange(format ? mask(text, format) : text)
                  getValue?.(format ? unMask(text) : text)
                }}
                value={value}
                style={styles.input}
                {...props}
              />
            </View>
            {errors?.[name] ? (
              <View style={styles.errorBox}>
                <Text style={styles.errorMessage}>{errors[name].message}</Text>
              </View>
            ) : (
              <></>
            )}
          </Fragment>
        )}
      />
    </View>
  )
}

export default FormatInput
