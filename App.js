import { SafeAreaProvider } from 'react-native-safe-area-context'
import { StatusBar } from 'expo-status-bar'
import Navigation from './src/navigations'
import { store } from './src/store'
import { Provider } from 'react-redux'

export default function App() {
  return (
    <SafeAreaProvider>
      <StatusBar style="auto" />
      <Provider store={store}>
        <Navigation />
      </Provider>
    </SafeAreaProvider>
  )
}
