import React, { useState, useEffect, useCallback } from 'react'
import { useFocusEffect } from '@react-navigation/native'
import { StyleSheet, View } from 'react-native'
import * as yup from 'yup'
import { mask, unMask } from 'react-native-mask-text'
import Form from '../../components/Form'
import FormatInput from '../../components/FormatInput'
import SubmitButton from '../../components/SubmitButton'
import { useSelector, useDispatch } from 'react-redux'
import { add, update } from '../../store/models/membersSlice'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 30,
    paddingHorizontal: 30,
    backgroundColor: '#fff',
  },
  margin: {
    marginBottom: 24,
  },
  marginButton: {
    marginBottom: 40,
  },
})

const schema = yup.object().shape({
  name: yup.string().required('required'),
  govId: yup.string().required('required'),
  phoneNumber: yup.string().required('required'),
})

const AddEditScreen = ({ navigation, route }) => {
  const members = useSelector((state) => state.members.members)
  const dispatch = useDispatch()
  const [name, setName] = useState(null)
  const [govId, setGovId] = useState(null)
  const [phoneNumber, setPhoneNumber] = useState(null)
  const [disabled, setDisabled] = useState(true)

  useFocusEffect(
    useCallback(() => {
      if (route.params) {
        setName(route.params.data.name)
        setGovId(route.params.data.govId)
        setPhoneNumber(route.params.data.phoneNumber)
      }
    }, [route.params])
  )

  useEffect(() => {
    setDisabled(!(name && govId?.length >= 13 && phoneNumber?.length >= 10))
  }, [name, govId, phoneNumber])

  const onSubmit = async (data) => {
    const values = {
      name: data.name,
      govId: unMask(data.govId),
      phoneNumber: unMask(data.phoneNumber),
    }

    if (route.params) {
      const newList = members.map((item) =>
        item.govId === route.params.data.govId ? values : item
      )
      dispatch(update(newList))
    } else {
      dispatch(add(values))
    }
    navigation.navigate('home')
  }

  return (
    <View style={styles.container}>
      <Form
        schema={schema}
        defaultValues={
          route.params
            ? {
                name: route.params.data.name.toUpperCase(),
                govId: mask(route.params.data.govId, '9-9999-99999-99-9'),
                phoneNumber: mask(
                  route.params.data.phoneNumber,
                  '999-999-9999'
                ),
              }
            : {
                name: '',
                govId: '',
                phoneNumber: '',
              }
        }
      >
        <FormatInput
          name="name"
          placeholder="NAME"
          label={name ? 'NAME' : null}
          style={styles.margin}
          getValue={(text) => setName(text)}
        />
        <FormatInput
          format="9-9999-99999-99-9"
          name="govId"
          placeholder="ID"
          label={govId ? 'ID' : null}
          style={styles.margin}
          getValue={(text) => setGovId(text)}
          maxLength={17}
        />
        <FormatInput
          format="999-999-9999"
          name="phoneNumber"
          placeholder="PHONE NUMBER"
          label={phoneNumber ? 'PHONE NUMBER' : null}
          style={styles.marginButton}
          getValue={(text) => setPhoneNumber(text)}
          maxLength={12}
        />
        <SubmitButton
          onSubmit={onSubmit}
          title="Submit"
          disabled={disabled}
          center
          width={220}
        />
      </Form>
    </View>
  )
}

export default AddEditScreen
