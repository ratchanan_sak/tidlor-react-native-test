import React, { useEffect } from 'react'
import { FormProvider, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { View, StyleSheet } from 'react-native'

const Form = ({
  defaultValues = {},
  children,
  schema,
  height = 'auto',
  style,
  enableReinitial = false,
}) => {
  const methods = useForm({
    defaultValues,
    resolver: yupResolver(schema),
  })

  const styles = StyleSheet.create({
    container: {
      width: '100%',
      backgroundColor: '#fff',
      borderRadius: 8,
      height,
      ...style,
    },
  })

  useEffect(() => {
    if (enableReinitial) {
      methods.reset(defaultValues)
    }
  }, [defaultValues, enableReinitial])

  return (
    <FormProvider {...methods}>
      <View style={styles.container}>{children}</View>
    </FormProvider>
  )
}

export default Form
