import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  members: [],
}

export const membersSlice = createSlice({
  name: 'members',
  initialState,
  reducers: {
    add: (state, action) => {
      state.members.push(action.payload)
    },
    update: (state, action) => {
      state.members = action.payload
    },
  },
})

export const { add, update } = membersSlice.actions

export default membersSlice.reducer
