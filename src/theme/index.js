const color = {
  primary: '#007AFF',
  card: '#0070FF',
}

const theme = {
  color,
}

export default theme
