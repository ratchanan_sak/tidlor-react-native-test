import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import HomeScreen from '../screens/HomeScreen'
import AddEditScreen from '../screens/AddEditScreen'
import theme from '../theme'

const Stack = createNativeStackNavigator()

const Navigation = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="home"
        screenOptions={{
          headerShown: true,
          headerTintColor: '#fff',
          headerStyle: { backgroundColor: theme.color.primary },
          headerTitleAlign: 'center',
          headerBackTitle: 'Back',
          contentStyle: { overflow: 'hidden' },
        }}
      >
        <Stack.Screen
          name="home"
          component={HomeScreen}
          options={{ title: 'My Members' }}
        />
        <Stack.Screen
          name="add"
          component={AddEditScreen}
          options={{ title: 'Add new member' }}
        />
        <Stack.Screen
          name="edit"
          component={AddEditScreen}
          options={{ title: 'Edit member' }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Navigation
