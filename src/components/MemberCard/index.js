import {
  Text,
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native'
import theme from '../../theme'
import { capitalize, formatGovId, formatPhoneNumber } from '../../utils/text'
import editIcon from '../../../assets/images/edit.png'
import deleteIcon from '../../../assets/images/delete.png'
import { useDispatch } from 'react-redux'
import { update } from '../../store/models/membersSlice'

const styles = StyleSheet.create({
  card: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    paddingHorizontal: 20,
    backgroundColor: theme.color.card,
    borderRadius: 8,
    marginBottom: 15,
  },
  name: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 18,
    lineHeight: 24,
  },
  text: {
    color: '#fff',
    lineHeight: 24,
  },
  iconGroup: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  editIcon: {
    width: 20,
    height: 20,
  },
  deleteIcon: {
    width: 30,
    height: 30,
  },
})

const MemberCard = ({ navigation, data, members }) => {
  const dispatch = useDispatch()

  const onDelete = () => {
    Alert.alert(
      'Confirmation',
      `Are you sure to delete\n${capitalize(data.name)}`,
      [
        {
          text: 'Cancel',
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => {
            const newList = members.filter((item) => item.govId !== data.govId)
            dispatch(update(newList))
          },
        },
      ]
    )
  }

  return (
    <View style={styles.card}>
      <View>
        <Text style={styles.name} numberOfLines={1}>
          {capitalize(data?.name)}
        </Text>
        <Text style={styles.text} numberOfLines={1}>
          {formatGovId(data?.govId)}
        </Text>
        <Text style={styles.text} numberOfLines={1}>
          {formatPhoneNumber(data?.phoneNumber)}
        </Text>
      </View>
      <View style={styles.iconGroup}>
        <TouchableOpacity onPress={() => navigation.navigate('edit', { data })}>
          <Image
            source={editIcon}
            resizeMode="contain"
            style={styles.editIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={onDelete}>
          <Image
            source={deleteIcon}
            resizeMode="contain"
            style={styles.deleteIcon}
          />
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default MemberCard
