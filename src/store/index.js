import { configureStore } from '@reduxjs/toolkit'
import membersReducer from './models/membersSlice'

export const store = configureStore({
  reducer: {
    members: membersReducer,
  },
})
