import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import theme from '../../theme'

const Button = ({
  title,
  onPress,
  buttonType = 'primary',
  disabled = false,
  width,
  center = false,
  icon,
}) => {
  const styles = StyleSheet.create({
    button: {
      width: width ? width : '100%',
      backgroundColor: buttonType === 'primary' ? theme.color.primary : '#fff',
      paddingVertical: 10,
      paddingHorizontal: 30,
      borderRadius: 8,
      borderWidth: 1,
      borderColor: theme.color.primary,
      alignItems: 'center',
      alignSelf: center ? 'center' : 'flex-start',
    },
    title: {
      color: buttonType === 'primary' ? '#fff' : theme.color.primary,
      fontWeight: '600',
    },
    row: {
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    icon: {
      marginRight: 10,
    },
    disabled: {
      backgroundColor: '#C4C4C4',
      borderColor: '#C4C4C4',
    },
  })

  return (
    <TouchableOpacity
      onPress={() => {
        onPress?.()
      }}
      style={
        disabled ? { ...styles.button, ...styles.disabled } : styles.button
      }
      disabled={disabled}
    >
      {icon ? (
        <View style={styles.row}>
          <View style={styles.icon}>{icon}</View>
          <Text style={styles.title}>{title}</Text>
        </View>
      ) : (
        <Text style={styles.title}>{title}</Text>
      )}
    </TouchableOpacity>
  )
}

export default Button
