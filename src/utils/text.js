import { mask } from 'react-native-mask-text'

export const capitalize = (str) => {
  if (!str) {
    return ''
  }

  const arr = str.toLowerCase().split(' ')

  if (arr.length <= 1) {
    return `${arr[0][0].toUpperCase()}${arr[0].slice(1)}`
  }

  return `${arr[0][0].toUpperCase()}${arr[0].slice(
    1
  )} ${arr[1][0].toUpperCase()}${arr[1].slice(1)}`
}

export const formatGovId = (str) => {
  if (!str) {
    return ''
  }
  // return `${str[0]}-${str.slice(1, 5)}-${str.slice(5, 10)}-${str.slice(
  //   10,
  //   12
  // )}-${str[12]}`
  return mask(str, '9-9999-99999-99-9')
}

export const formatPhoneNumber = (str) => {
  if (!str) {
    return ''
  }
  // return `${str.slice(0, 3)}-${str.slice(3, 6)}-${str.slice(6)}`
  return mask(str, '999-999-9999')
}
