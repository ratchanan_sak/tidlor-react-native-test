import React from 'react'
import { StyleSheet, View, FlatList, Image } from 'react-native'
import Button from '../../components/Button'
import MemberCard from '../../components/MemberCard'
import plusIcon from '../../../assets/images/plus.png'
import { useSelector } from 'react-redux'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 30,
    paddingHorizontal: 20,
    backgroundColor: '#fff',
  },
  listContainer: {
    width: '100%',
    flexDirection: 'column',
    marginBottom: 50,
  },
  buttonContainer: {
    width: '100%',
    marginVertical: 15,
  },
  emptyContainer: {
    flex: 1,
    alignItems: 'center',
    marginTop: '20%',
  },
  icon: {
    width: 16,
    height: 16,
  },
})

const HomeScreen = ({ navigation }) => {
  const members = useSelector((state) => state.members.members)

  return (
    <View style={styles.container}>
      {members?.length > 0 ? (
        <View style={styles.listContainer}>
          <FlatList
            data={members}
            renderItem={({ item, index }) => (
              <MemberCard
                key={`${index}`}
                navigation={navigation}
                data={item}
                members={members}
              />
            )}
          />
          <View style={styles.buttonContainer}>
            <Button
              title="Add member"
              buttonType="secondary"
              icon={
                <Image
                  source={plusIcon}
                  resizeMode="contain"
                  style={styles.icon}
                />
              }
              onPress={() => navigation.navigate('add')}
            />
          </View>
        </View>
      ) : (
        <View style={styles.emptyContainer}>
          <Button
            title="Add your first member"
            center
            width={220}
            onPress={() => navigation.navigate('add')}
          />
        </View>
      )}
    </View>
  )
}

export default HomeScreen
