import React from 'react'
import { useFormContext } from 'react-hook-form'
import Button from '../Button'

const SubmitButton = ({ title, disabled = false, onSubmit, ...rest }) => {
  const { handleSubmit, reset } = useFormContext()

  return (
    <Button
      title={title}
      buttonType="primary"
      onPress={handleSubmit((data, e) => {
        onSubmit(data, e, reset)
      })}
      disabled={disabled}
      {...rest}
    />
  )
}

export default SubmitButton
